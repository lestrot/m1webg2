<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Product;
use Faker\Factory as Faker;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    	// instancier faker
	    $faker = Faker::create('fr_FR');

		// pour remplir la table, créer des objets puis les persister
	    for($i = 0; $i < 20; $i++){
		    $product = new Product();
		    $product
			    ->setDescription($faker->text)
			    ->setImage($faker->image('public/img/product/', 800, 450, null, false))
			    ->setName($faker->unique()->sentence(5))
			    ->setPrice($faker->randomFloat(2, 1, 999.99))
		    ;

		    // récupération d'une référence créée dans CategoryFixtures
		    $randomCategory = random_int(0, 4);
		    $product->addCategory( $this->getReference("category$randomCategory") );

		    // persist : créer un enregistrement
		    $manager->persist($product);
	    }

	    // flush : exécuter les requêtes sql
        $manager->flush();
    }
}
