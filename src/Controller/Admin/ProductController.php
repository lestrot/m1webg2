<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Service\FileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// préfixe de toutes les routes contenues dans le contrôleur
/**
 * @Route("/admin/product")
 */
class ProductController extends AbstractController
{
	/**
	 * @Route("/", name="admin.product.index")
	 */
	public function index(ProductRepository $productRepository):Response
	{
		// récupération de tous les résultats
		$results = $productRepository->findAll();

		return $this->render('admin/product/index.html.twig', [
			'results' => $results
		]);
	}

	/**
	 * @Route("/form", name="admin.product.form")
	 * @Route("/form/update/{id}", name="admin.product.form.update")
	 */
	public function form(Request $request, EntityManagerInterface $entityManager, int $id = null, ProductRepository $productRepository):Response
	{
		// si l'id est nul, une insertion est exécutée, sinon une modification est exécutée
		$model = $id ? $productRepository->find($id) : new Product();
		$type = ProductType::class;
		$form = $this->createForm($type, $model);
		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			//dd($form->getData());

			// message de confirmation
			$message = $model->getId() ? "Le produit a été modifié" : "Le produit a été ajouté";

			// message stocké en session
			$this->addFlash('notice', $message);

			/*
			 * insertion dans la base de données
			 *  - persist: méthode déclenchée uniquement lors d'une insertion
			 *  - lors d'une mise à jour, aucune méthode n'est requise
			 *  - remove: méthode déclenchée uniquement lors d'une suppression
			 *  - flush: exécution des requêtes SQL
			 */
			$model->getId() ? null : $entityManager->persist($model);
			$entityManager->flush();

			// redirection
			return $this->redirectToRoute('admin.product.index');
		}

		return $this->render('admin/product/form.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/remove/{id}", name="admin.product.remove")
	 */
	public function remove(ProductRepository $productRepository, EntityManagerInterface $entityManager, int $id, FileService $fileService):Response
	{
		// autoriser la route uniquement aux super admin
		if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$this->addFlash('error', "Vous n'êtes pas autorisé à supprimer un produit");
			return $this->redirectToRoute('admin.product.index');
		}

		// sélection de l'entité à supprimer
		$model = $productRepository->find($id);

		// suppression dans la table
		$entityManager->remove($model);
		$entityManager->flush();

		// suppression de l'image
		if(file_exists("img/product/{$model->getImage()}")){
			$fileService->remove('img/product', $model->getImage());
		}

		// message et redirection
		$this->addFlash('notice', "Le produit a été supprimé");
		return $this->redirectToRoute('admin.product.index');
	}

}












