<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{
	/**
	 * @Route("/layout", name="hello.layout")
	 */
	public function layout():Response
	{
		return $this->render('hello/layout.html.twig');
	}




	/*
	 * dans les annotations : utiliser uniquement des doubles guillemets
	 * Route: paramètres
	 *  - schéma de la route: url
	 *      variables de route sont entourées par des accolades
	 *      les variables de route deviennent des paramètres de la méthode
	 *  - name: identifiant unique de la route
	 *      nomenclature : nom du contrôleur.nom de la méthode
	 *  - requirements : définir une expression rationnelle aux variables de route
	 */

	/**
	 * @Route(
	 *     "/hello/{firstname}",
	 *     name="hello.index",
	 *     requirements={ "firstname"="[a-z-\s]+" }
	 * )
	 */
	public function index(?string $firstname = null):Response
	{
		//dd($firstname);
		/*
		 * render : paramètres
		 *  - emplacement de la vue située dans le dossier templates
		 *    architecture recommandée :
		 *      - dossier reprend le nom du contrôleur
		 *      - vue reprend le nom de la méthode
		 *  - variables envoyées à la vue sous forme de tableau associatif
		 *      les clés deviennent des variables dans la vue
		 */
		return $this->render('hello/index.html.twig', [
			'firstname' => $firstname,
			'list' => [
				'key0' => 'value0',
				'key1' => 'value1',
				'key2' => 'value2',
			],
			'now' => new \DateTime(),
			'price' => 45000.80
		]);
	}
}





