<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function testDQL():Query
    {
    	// requête
	    /*
	     * méthode createQueryBuilder : écrire une requête en DQL
	     *  création d'un alias
	     * méthode getQuery : à utiliser obligatoirement en fin de requête
	     * select: sélectionner certaines propriétés de l'entité à l'aide de l'alias
	     *   pas d'astérisque: utilisation de l'alias
	     * from est implicite et relié au repository
	     * where et andWhere : condition
	     *      utiliser where pour la première condition
	     *      utiliser andWhere pour les suivantes
	     * paramètres des conditions sont préfixés par :
	     *      utiliser setParameters pour spécifier la valeur aux paramètres
	     * setMaxResults : équivalent de LIMIT
	     * setFirstResult : équivalent à OFFSET
	     * join: jointure interne
	     *   cibler une propriété relationnelle de l'entité
	     *   création d'un alias de l'entité en relation
	     *   pas de ON
	     * tri: orderBy et addOrderBy
	     * */
    	$query = $this->createQueryBuilder('product')
		    //->select('product') // équivalent à product.*

		    /*->where('product.price >= :price')
		    ->andWhere('product.name LIKE :search')
		    ->setParameters([
		    	'price' => 100,
			    'search' => '%non%'
		    ])*/
			/*->select('product.id, product.name, product.price, categories.name AS cname')
		    ->where('REGEXP(product.id, :pattern) = true')
		    ->join('product.categories', 'categories')
		    ->setMaxResults(3)
		    ->setFirstResult(5)
		    ->setParameters([
		    	'pattern' => '^1'
		    ])*/
			/*->select('categories.name, COUNT(product.id) AS total')
		    ->join('product.categories', 'categories')
		    ->where('categories.id IN (1,2)')
		    ->groupBy('categories.name')*/
			->select('categories.name,
				GROUP_CONCAT(product.id) groupid,
				COUNT(product.id) AS total'
		    )
			->join('product.categories', 'categories')
		    ->groupBy('categories.name')
		    ->orderBy('total', 'DESC')
			->getQuery()
	    ;

    	// retour de la requête
    	return $query;
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
